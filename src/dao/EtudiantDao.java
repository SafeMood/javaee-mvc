package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.entities.Etudiant;
import utility.SingletonConnexion;

public class EtudiantDao implements InterDao<Etudiant> {

	private Connection connexion = SingletonConnexion.getConnexion();


 	@Override
	public List<Etudiant> listerElement() {
 		List<Etudiant> ListEtudiant = new ArrayList<Etudiant>();
		try {
			Statement stmt = connexion.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM etudiant");

			while (rs.next()) {

				String nom = rs.getString(2);
				String prenom = rs.getString(3);
				String adresse = rs.getString(4);
				String tel = rs.getString(5);
				String email = rs.getString(6);
				String numcarte = rs.getString(7);
				String ncin = rs.getString(8);
				Etudiant etudiant = new Etudiant(nom, prenom, adresse, tel, numcarte, email, ncin);
 				ListEtudiant.add(etudiant);
 
			}

			rs.close();
			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return  ListEtudiant;
	}

	@Override
	public void ajouterElement(Etudiant etudiant) {
		try {
			PreparedStatement ps = connexion.prepareStatement(
					"insert into etudiant (nom, prenom, adresse, tel, numcarte, email, ncin) values(?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, etudiant.getNom());
			ps.setString(2, etudiant.getPrenom());
			ps.setString(3, etudiant.getAdresse());
			ps.setString(4, etudiant.getTel());
			ps.setString(5, etudiant.getEmail());
			ps.setString(6, etudiant.getNumcarte());
			ps.setString(7, etudiant.getNcin());

			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void modifierElement(Etudiant etudiant) {

		try {
			PreparedStatement ps = connexion.prepareStatement(
					"UPDATE etudiant SET nom = ?, prenom = ?, adresse = ?, tel = ?, email = ?, numcarte = ?   WHERE ncin=?");

			ps.setString(1, etudiant.getNom());
			ps.setString(2, etudiant.getPrenom());
			ps.setString(3, etudiant.getAdresse());
			ps.setString(4, etudiant.getTel());
			ps.setString(5, etudiant.getEmail());
			ps.setString(6, etudiant.getNumcarte());
			ps.setString(7, etudiant.getNcin());

			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void supprimerElement(Etudiant etudiant) {
		try {
			PreparedStatement preparedStatement = connexion.prepareStatement("DELETE FROM etudiant WHERE ncin=?");
			preparedStatement.setString(1, etudiant.getNcin());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
