package dao;

import java.util.List;


public interface InterDao <Element> {
 		
	public List<Element> listerElement();
	public void ajouterElement(Element e);
	public void modifierElement(Element e);
	public void supprimerElement(Element e);
		
}
