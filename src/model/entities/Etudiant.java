package model.entities;

import java.util.List;

public class Etudiant {
	private Long id;
	private String nom;
	private String prenom;
	private String tel;
	private String adresse;
	private String ncin;
	private String numcarte;
	private String email;
	private List<Enseignant> lstens;

	public Etudiant() {
		super();
	}

	public Etudiant(String nom, String prenom, String adresse, String tel, String numcarte, String email, String ncin) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.tel = tel;
		this.email = email;
		this.numcarte = numcarte;
		this.ncin = ncin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getNcin() {
		return ncin;
	}

	public void setNcin(String ncin) {
		this.ncin = ncin;
	}

	public String getNumcarte() {
		return numcarte;
	}

	public void setNumcarte(String numcarte) {
		this.numcarte = numcarte;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
