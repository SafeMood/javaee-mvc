package model.metier;

import java.util.List;

import dao.EtudiantDao;
import model.entities.Etudiant;

public class EtudiantModel {

	private Etudiant etudiant;
	private EtudiantDao etudiantService = new EtudiantDao();

	public List<Etudiant> lister() {

		return this.etudiantService.listerElement();
	}

	public void ajouter() {

		this.etudiantService.ajouterElement(etudiant);
	}

	public void modifier() {
		this.etudiantService.modifierElement(etudiant);
	}

	public void supprimer() {
		this.etudiantService.supprimerElement(etudiant);
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

}
