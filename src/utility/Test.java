package utility;

import java.util.List;

import model.entities.Etudiant;
import model.metier.EtudiantModel;

public class Test {

	public static void main(String[] args) {

		EtudiantModel etudiantModel = new EtudiantModel();

		try {

			List<Etudiant> listEtudiant = etudiantModel.lister();

			for (Etudiant etudiant : listEtudiant) {

				System.out.println(etudiant.getNom());
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
}
