package utility;

import java.sql.Connection;
import java.sql.DriverManager;
//import java.sql.SQLException;

public class SingletonConnexion {
	private static Connection connexion;
	// bloc static
	static {
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/bdEtudiant", "root", "");
			System.out.println("connexion etablie");

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	public static Connection getConnexion() {
		return connexion;
	}
}
