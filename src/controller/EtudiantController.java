package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.entities.Etudiant;
import model.metier.EtudiantModel;

/**
 * Servlet implementation class EtudiantController
 */
@WebServlet("/EtudiantController")
public class EtudiantController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EtudiantController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		EtudiantModel modeleEtudiant = new EtudiantModel();

		if (request.getParameter("action").equals("Ajouter")) {

			String nom = request.getParameter("nom");
			String prenom = request.getParameter("prenom");
			String adresse = request.getParameter("adress");
			String tel = request.getParameter("tel");
			String numcarte = request.getParameter("numC");
			String email = request.getParameter("email");
			String ncin = request.getParameter("ncin");

			Etudiant etudiant = new Etudiant(nom, prenom, adresse, tel, numcarte, email, ncin);

			modeleEtudiant.setEtudiant(etudiant);

			modeleEtudiant.ajouter();

			response.getWriter().append("Ajouter avec succès !");
		}

		if (request.getParameter("action").equals("Modifier")) {

			String nom = request.getParameter("nom");
			String prenom = request.getParameter("prenom");
			String adresse = request.getParameter("adress");
			String tel = request.getParameter("tel");
			String numcarte = request.getParameter("numC");
			String email = request.getParameter("email");
			String ncin = request.getParameter("ncin");

			Etudiant etudiant = new Etudiant(nom, prenom, adresse, tel, numcarte, email, ncin);

			modeleEtudiant.setEtudiant(etudiant);

			modeleEtudiant.modifier();

			response.getWriter().append("Modifier avec succès !");
		}
		if (request.getParameter("action").equals("Supprimer")) {

			String ncin = request.getParameter("ncin");

			Etudiant etudiant = new Etudiant();

			etudiant.setNcin(ncin);

			modeleEtudiant.setEtudiant(etudiant);

			modeleEtudiant.supprimer();

			response.getWriter().append("suppression avec succès !");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
